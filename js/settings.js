window.addEventListener('DOMContentLoaded', function() {
	document.querySelector('#change-password form').addEventListener('submit', function(e) {
		e.preventDefault();

		ajax('Profile.aspx?action=ChangePassword', new FormData(this), function(response) {
			if(!response.Status) {
				showMessage('warning', response.Response, 'change-password');
				return;
			}

			showMessage('notice', 'Password changed successfuly!', 'change-password');
		}, 'POST');
	});

	document.querySelector('#delete-profile form').addEventListener('submit', function(e) {
		e.preventDefault();

		ajax('Profile.aspx?action=Delete', new FormData(this), function(response) {
			if(!response.Status) {
				showMessage('warning', response.Response, 'delete-profile');
				return;
			}

			logout();
		}, 'POST');
	});

	document.getElementById('sessions-reset').addEventListener('click', () => ajax('Sessions.aspx?action=Reset', null, loadSessions, 'POST'));

	loadSessions();
});

function loadSessions() {
	ajax('Sessions.aspx?action=Get', null, function(response) {
		var container = document.querySelector('#sessions tbody'),
			sessions = response.Response;

		container.textContent = '';

		for(var i = 0; i < sessions.length; i++) {
			var row = document.createElement('tr'),
				ip = document.createElement('td'),
				timestamp = document.createElement('td'),
				lastused = document.createElement('td'),
				useragent = document.createElement('td');

			row.appendChild(ip);
			row.appendChild(timestamp);
			row.appendChild(lastused);
			row.appendChild(useragent);

			container.appendChild(row);

			ip.textContent = sessions[i].ip;
			timestamp.textContent = sessions[i].timestamp;
			lastused.textContent = sessions[i].lastused;
			useragent.textContent = sessions[i].useragent;

			if(sessions[i].current == 'true')
				row.classList.add('current');
		}
	});
}

function showMessage(type, message, insertBefore) {

	if(document.querySelector('.notice'))
		document.querySelector('.notice').parentElement.removeChild(document.querySelector('.notice'));

	if(document.querySelector('.warning'))
		document.querySelector('.warning').parentElement.removeChild(document.querySelector('.warning'));

	var div = document.createElement('div');
	div.classList.add(type);
	div.textContent = message;
	document.getElementById('settings').insertBefore(div, document.getElementById(insertBefore));
}