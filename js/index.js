window.addEventListener('DOMContentLoaded', function() {
	posts.update();
});

var posts = {
	update: function() {
		ajax('Posts.aspx?action=Get', 'orderby=time&order=asc&start=0&count=20', function(response) {
			if(!response.Status)
				return;

			loadPosts(response.Response, document.getElementById('posts'), posts.update);
		});
	}
}