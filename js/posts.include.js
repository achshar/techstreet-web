function loadPosts(posts, container, reload) {

	container.textContent = '';

	if(!posts.length) {
		container.innerHTML = '<section class="NA">No posts found! :(</section>';
		return;
	}

	for(var i = 0; i < posts.length; i++) {

		/* Declare */
		var post = document.createElement('section'),

			votes = document.createElement('div'),
			upvote = document.createElement('div'),
			votecount = document.createElement('div'),
			downvote = document.createElement('downvote'),

			info = document.createElement('div'),
			topinfobar = document.createElement('div'),
			username = document.createElement(posts[i].username ? 'a' : 'span'),
			separator1 = document.createElement('span'),
			timestamp = document.createElement('span'),
			separator2 = document.createElement('span'),
			Delete = document.createElement('a'),
			title = document.createElement('a'),
			bottominfobar = document.createElement('div'),
			comments = document.createElement('a');

		/* Append */
		container.appendChild(post);

		post.appendChild(votes);
		post.appendChild(info);

		votes.appendChild(upvote);
		votes.appendChild(votecount);
		votes.appendChild(downvote);

		info.appendChild(topinfobar);
		info.appendChild(title);
		info.appendChild(bottominfobar);

		topinfobar.appendChild(username);
		topinfobar.appendChild(separator1)
		topinfobar.appendChild(timestamp);

		if(posts[i].username == localStorage['username']) {
			topinfobar.appendChild(separator2)
			topinfobar.appendChild(Delete);
		}

		bottominfobar.appendChild(comments);

		/* Assign Classes */
		post.classList.add('post');

		votes.classList.add('votes');
		upvote.classList.add('upvote');
		downvote.classList.add('downvote');
		votecount.classList.add('votecount');

		if(posts[i].uservote == 1)
			upvote.classList.add('voted');
		else if(posts[i].uservote == -1)
			downvote.classList.add('voted');

		topinfobar.classList.add('infobar');
		title.classList.add('title');
		bottominfobar.classList.add('infobar');

		/* Add Valus */
		votecount.textContent = parseInt(posts[i].votes) || 0;

		username.textContent = posts[i].username || '[deleted]';
		if(posts[i].username)
			username.setAttribute('href', '/profile.html?username='+posts[i].username);

		separator1.innerHTML = '&bull;';
		timestamp.textContent = timeSince(posts[i].timestamp);
		timestamp.setAttribute('title', posts[i].timestamp);

		if(posts[i].username == localStorage['username']) {
			separator2.innerHTML = '&bull;';
			Delete.textContent = 'delete';
		}

		title.textContent = posts[i].title;
		title.setAttribute('href', posts[i].url || '/post.html?id='+posts[i].id);

		comments.textContent = posts[i].comments+' comments';
		comments.setAttribute('href', '/post.html?id='+posts[i].id);

		/* Event Listeners */
		(function(post, upvote, downvote, votecount, Delete) {

			upvote.addEventListener('click', function() {

				votecount.textContent = parseInt(votecount.textContent) + (this.classList.contains('voted') ? -1 : downvote.classList.contains('voted') ? 2 : 1);

				this.classList.toggle('voted');
				downvote.classList.remove('voted');

				var formData = new FormData();
				formData.append('post', post.id);
				formData.append('vote', '1');
				ajax('Post.aspx?action=Vote', formData, null, 'POST');
			});

			downvote.addEventListener('click', function() {

				votecount.textContent = parseInt(votecount.textContent) + (this.classList.contains('voted') ? 1 : upvote.classList.contains('voted') ? -2 : -1);

				this.classList.toggle('voted');
				upvote.classList.remove('voted');

				var formData = new FormData();
				formData.append('post', post.id);
				formData.append('vote', '-1');
				ajax('Post.aspx?action=Vote', formData, null, 'POST');
			});

			if(post.username == localStorage['username']) {
				Delete.addEventListener('click', function() {
					var formData = new FormData();
					formData.append('id', post.id);
					ajax('Post.aspx?action=Delete', formData, reload, 'POST');
				});
			}

		})(posts[i], upvote, downvote, votecount, Delete);
	}
}