var timeOutLoader = false,
	authenticateUser = true;

window.addEventListener('DOMContentLoaded', function() {

	if(authenticateUser && !localStorage['token'])
		logout();

	var loader = document.createElement('div');
	loader.setAttribute('id', 'loader');
	document.querySelector('header').appendChild(loader);

	if(document.getElementById('user-info'))
		document.querySelector('#user-info a').textContent = localStorage['username'];

	if(document.getElementById('logout'))
		document.getElementById('logout').addEventListener('click', logout);
});

function ajax(url, parameters, callback, method, noLoader) {

	url = 'http://localhost:8080/'+url;

	if(authenticateUser)
		url += '&token='+localStorage['token'];

	var loader = document.getElementById('loader');

	if(loader) {
		if(timeOutLoader)
			clearTimeout(timeOutLoader);

		timeOutLoader = setTimeout(function() {
			loader.innerHTML = 'Still working&hellip;';
			loader.style.marginLeft = Math.round(-loader.clientWidth / 2)+'px';
		}, 5000);

		loader.innerHTML = 'Working&hellip;';
		loader.style.marginLeft = Math.round(-loader.clientWidth / 2)+'px';
		loader.style.opacity = 1;
	}

	if(method != 'POST')
		method = 'GET';

	var call = new XMLHttpRequest(),
		send = null;

	if(parameters) {
		(method == 'POST' || typeof parameters != 'string') ? send = parameters : url = url + '&' + parameters;
	}

	call.onreadystatechange = function() {
		if(call.readyState != 4)
			return;

		document.getElementById('loader').style.opacity = 0;

		var response = JSON.parse(call.responseText);

		if(!response.Status && response.Response == "Invalid Token! :(") {
			window.location = '/login.html'
			return;
		}

		if(callback)
			callback(response);
	};

	call.open(method, url, true);

	if(method == 'POST' && typeof parameters == 'string')
		call.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	call.send(send);

	return call;
}

function timeSince(timeStamp) {

	var now = new Date(),
		timeStamp = new Date(timeStamp+'+05:30'),
		secondsPast = (now.getTime() - timeStamp.getTime()) / 1000;

	if(secondsPast < 60)
		return parseInt(secondsPast) + 's';

	if(secondsPast < 3600)
		return parseInt(secondsPast / 60) + 'm';

	if(secondsPast <= 86400)
		return parseInt(secondsPast / 3600) + 'h';

	if(secondsPast > 86400){
		day = timeStamp.getDate();
		month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(' ','');
		year = timeStamp.getFullYear() == now.getFullYear() ? '' :  ' '+timeStamp.getFullYear();

		return day + ' ' + month + year;
	}
}

function _GET(variable) {
	var vars = window.location.search.substring(1).split("&");
	for(var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable)
			return pair[1];
	}
	return false;
}

function logout() {
	ajax('Sessions.aspx?action=Purge', null, () => window.location = '/login.html', 'POST');
}