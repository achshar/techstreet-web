if(!_GET('id'))
	window.location = '/';

window.addEventListener('DOMContentLoaded', function() {
	document.querySelector('#add-post-comment form').addEventListener('submit', function(e) {
		e.preventDefault();
		post.comments.add();
	});

	post.load();
	post.comments.load();
});

var post = {
	load: function() {
		ajax('Posts.aspx?action=Get', 'id='+_GET('id'), function(response) {
			if(!response.Status)
				return;

			loadPosts(response.Response, document.getElementById('post-information'), post.load);

			if(response.Response[0].body)
				document.getElementById('post-body').textContent = response.Response[0].body;
			else
				document.getElementById('post-body').classList.add('hidden');
		});
	},
	comments: {
		add: function() {
			var form = document.querySelector('#add-post-comment form'),
				formData = new FormData(form);

			form.reset();
			formData.append('post', _GET('id'));

			ajax('Comment.aspx?action=Insert', formData, post.comments.load, 'POST');
		},
		load: function() {
			ajax('Comments.aspx?action=Get', 'post='+_GET('id'), function(response) {
				if(!response.Status)
					return;

				loadComments(response.Response, document.getElementById('comments'), post.comments.load);
			});
		}
	}
};