window.addEventListener('DOMContentLoaded', function() {
	profile.load();
});

var profile = {
	load: function() {

		ajax('Profile.aspx?action=Get', _GET('username') ? 'username='+_GET('username') : null, function(response) {

			if(!response.Status)
				return;

			var user = response.Response[0];

			for(var property in user) {
				if(!user.hasOwnProperty(property))
					continue;

				if(document.getElementById('info-'+property))
					document.getElementById('info-'+property).textContent = user[property];
			}

			document.getElementById('info-timestamp').textContent = timeSince(user.timestamp);
			document.getElementById('info-lastlogin').textContent = timeSince(user.lastlogin);

			profile.loadPosts();
			profile.loadComments();
		});
	},
	loadPosts: function() {
		ajax('Posts.aspx?action=Get', 'orderby=time&order=asc&start=0&count=20&username='+(_GET('username') || localStorage['username']), function(response) {
			if(response.Status)
				loadPosts(response.Response, document.getElementById('posts'), profile.loadPosts);
		});
	},
	loadComments: function() {
		ajax('Comments.aspx?action=Get', 'username='+(_GET('username') || localStorage['username']), function(response) {
			if(response.Status)
				loadComments(response.Response, document.getElementById('comments'), profile.loadComments, true);
		});
	}
}