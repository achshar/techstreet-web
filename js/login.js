authenticateUser = false;

window.addEventListener('DOMContentLoaded', function() {

	document.querySelector('form').addEventListener('submit', function(e) {
		e.preventDefault();

		showMessage('notice', 'Logging you in...')

		ajax('user.aspx?action=Login', new FormData(this), function(response) {
			if(response.Status) {
				showMessage('notice', 'Login successful. Redirecting...');
				localStorage['username'] = document.querySelector('form').elements['username'].value;
				localStorage['token'] = response.Response;
				window.location = '/';
			} else {
				showMessage('warning', response.Response);
			}
		}, 'POST');
	});

	document.getElementById('button-register').addEventListener('click', function() {
		showMessage('notice', 'Registering...')

		ajax('user.aspx?action=Register', new FormData(document.querySelector('form')), function(response) {
			if(response.Status) {
				showMessage('notice', 'Registration successful. You can log in now.');
			} else {
				showMessage('warning', response.Response);
			}
		}, 'POST');
	});
});

function showMessage(type, message) {

	if(document.querySelector('.notice'))
		document.querySelector('.notice').parentElement.removeChild(document.querySelector('.notice'));

	if(document.querySelector('.warning'))
		document.querySelector('.warning').parentElement.removeChild(document.querySelector('.warning'));

	var div = document.createElement('div');
	div.classList.add(type);
	div.textContent = message;
	document.querySelector('form').insertBefore(div, document.querySelector('section'));
}