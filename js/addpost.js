window.addEventListener('DOMContentLoaded', function() {
	document.querySelector('form').addEventListener('submit', function(e) {
		e.preventDefault();

		ajax('post.aspx?action=insert', new FormData(this), function(response) {
			if(response.Status && response.Response)
				window.location = '/post.html?id='+response.Response;
		}, 'POST');
	});
});