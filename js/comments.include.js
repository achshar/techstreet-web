function loadComments(comments, container, reload, showPost) {

	container.textContent = '';

	if(!comments.length) {
		container.innerHTML = '<section class="NA">No comments found! :(</section>';
		return;
	}

	for(var i = 0; i < comments.length; i++) {

		/* Declare */
		var comment = document.createElement('section'),

			votes = document.createElement('div'),
			upvote = document.createElement('div'),
			votecount = document.createElement('div'),
			downvote = document.createElement('downvote'),

			info = document.createElement('div'),
			topinfobar = document.createElement('div'),
			username = document.createElement(comments[i].username ? 'a' : 'span'),
			separator1 = document.createElement('span'),
			timestamp = document.createElement('span');
			separator2 = document.createElement('span'),
			Delete = document.createElement('a'),

			body = document.createElement('div'),

			bottominfobar = document.createElement('div'),
			post = document.createElement('a');

		/* Append */
		container.appendChild(comment);

		comment.appendChild(votes);
		comment.appendChild(info);

		votes.appendChild(upvote);
		votes.appendChild(votecount);
		votes.appendChild(downvote);

		info.appendChild(topinfobar);
		info.appendChild(body);
		info.appendChild(bottominfobar);

		topinfobar.appendChild(username);
		topinfobar.appendChild(separator1)
		topinfobar.appendChild(timestamp);

		if(comments[i].username == localStorage['username']) {
			topinfobar.appendChild(separator2)
			topinfobar.appendChild(Delete);
		}

		if(showPost)
			bottominfobar.appendChild(post);

		/* Assign Classes */
		comment.classList.add('comment');

		votes.classList.add('votes');
		upvote.classList.add('upvote');
		downvote.classList.add('downvote');
		votecount.classList.add('votecount');

		if(comments[i].uservote == 1)
			upvote.classList.add('voted');
		else if(comments[i].uservote == -1)
			downvote.classList.add('voted');

		topinfobar.classList.add('infobar');
		body.classList.add('body');
		bottominfobar.classList.add('infobar');

		/* Add Valus */
		votecount.textContent = parseInt(comments[i].votes) || 0;

		username.textContent = comments[i].username || '[deleted]';
		if(comments[i].username)
			username.setAttribute('href', '/profile.html?username='+comments[i].username);

		separator1.innerHTML = '&bull;';
		timestamp.textContent = timeSince(comments[i].timestamp);
		timestamp.setAttribute('title', comments[i].timestamp);

		if(comments[i].username == localStorage['username']) {
			separator2.innerHTML = '&bull;';
			Delete.textContent = 'delete';
		}

		body.textContent = comments[i].username ? comments[i].body : '[deleted]';
		if(!comments[i].username)
			body.classList.add('deleted');

		if(showPost) {
			post.textContent = comments[i].posttitle;
			post.setAttribute('href', '/post.html?id='+comments[i].post);
		}

		/* Event Listeners */
		(function(comment, upvote, downvote, votecount, Delete) {

			upvote.addEventListener('click', function() {

				votecount.textContent = parseInt(votecount.textContent) + (this.classList.contains('voted') ? -1 : downvote.classList.contains('voted') ? 2 : 1);

				this.classList.toggle('voted');
				downvote.classList.remove('voted');

				var formData = new FormData();
				formData.append('post', comment.id);
				formData.append('vote', '1');
				ajax('Comment.aspx?action=Vote', formData, null, 'POST');
			});

			downvote.addEventListener('click', function() {

				votecount.textContent = parseInt(votecount.textContent) + (this.classList.contains('voted') ? 1 : upvote.classList.contains('voted') ? -2 : -1);

				this.classList.toggle('voted');
				upvote.classList.remove('voted');

				var formData = new FormData();
				formData.append('post', comment.id);
				formData.append('vote', '-1');
				ajax('Comment.aspx?action=Vote', formData, null, 'POST');
			});

			if(comment.username == localStorage['username']) {
				Delete.addEventListener('click', function() {
					var formData = new FormData();
					formData.append('id', comment.id);
					ajax('Comment.aspx?action=Delete', formData, reload, 'POST');
				});
			}

		})(comments[i], upvote, downvote, votecount, Delete);
	}
}